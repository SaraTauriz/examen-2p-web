xports.register = (req, res) =>
{
        const {cedula,apellidos,nombres,direccion,semestre,paralelo} = req.body;
        
        database.query('INSERT INTO mp_usuario SET ?', 
        {
            cedula: cedula,
            usuario_apellidos: apellidos,
            usuario_nombres: nombres,
            usuario_direccion: direccion,
            usuario_semestre: semestre,
            usuario_paralelo: paralelo, 
        }, (error, results) => 
        {
            if(error)
            {
                throw error;
            }
            else
            {
                req.session.successful = 'Usuario registrado exitosamente.';
                return;
            }
        });
    };
